import numpy as np
from keras.layers import LSTM, Dense, Input
from keras.models import Model
from keras.models import load_model
# from keras.utils import plot_model

import sys
import json
path = "../MLDS_hw2_1_data/"
print(sys.argv)
try:
    path2='./models/'+sys.argv[1]+'/'
except:
    path2='./models/less_embedding_dim/'
print(path2)
def progress(count, total, status=""):
    """ progress bar, just for fun """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s/%s ...%s\r" % (bar, count, total, status))
    sys.stdout.flush()


model = load_model(path2+'seq3seq.h5')
# plot_model(model, to_file='model.png')
print(model.summary())
token_num, max_len = 0, 0
""" load parameters """
with open(path2+'parameters.json', 'r') as file:
    labels = json.load(file)
    k = labels['input2_shape']
    max_len = k[1]
    token_num = k[2]

""" load training data """
ids = []
with open(path + "testing_label.json") as file:
    labels = json.load(file)
    for i in labels:
        ids.append(i['id'])


data = []
for k, id in enumerate(ids):
    feat = np.load(path + "testing_data/feat/" + id + ".npy")
    progress(k + 1, len(ids), " loading data ")
    data.append(feat)

data = np.array(data)
with open(path2+'idx2word.json', 'r') as file:
    idx2word = json.load(file)


def decode2word(a):
    try:
        # b = idx2word[a]
        b = idx2word[str(np.argmax(a))]+' '
#         if b != '<PAD> ':
            # print(b, end=' ')
#             pass
#         else:
#             b = ''
    except:
        b = '<UWK> '
        print('<UNK>', end=' ')
    return b


outputs = []
unused_input = np.zeros((data.shape[0], max_len, token_num))
print()
for i in model.predict([data, unused_input]):
    temp = ''
    for k in i:
        try:
            b = idx2word[str(np.argmax(k))]+' '
#             b = idx2word[k]+' '
            if b != '<PAD> ':
                #print(b, end=' ')
                pass
            else:
#               break
                b=''
#                 pass
        except:
            b = '<UWK> '

            print('<UNK>', end=' ')
        temp += b
    if temp!="":
        temp = temp[0].upper()+temp[1:]
    print(temp)
    outputs.append(temp)
with open(path2+'output_testset.txt', 'w') as file:
    for i in range(len(outputs)):
        output = ids[i]+','+outputs[i]+'.\n'
        file.write(output)
