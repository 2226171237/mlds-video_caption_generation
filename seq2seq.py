import json
import re

# import keras.preprocessing.text as T
import numpy as np
from keras.layers import Dense, Input, GRU#, LSTM
from keras.models import Model

# from keras.preprocessing.text import Tokenizer
import tensorflow as tf

import sys
import random

batch_size = 64
epochs = 100
latent_dim = 256
path = "../MLDS_hw2_1_data/"
path2="./models/gru/"

def progress(count, total, status=""):
    """ progress bar, just for fun """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s/%s ...%s\r" % (bar, count, total, status))
    sys.stdout.flush()


""" load training data """
with open(path + "training_label.json") as file:
    labels = json.load(file)
    label_dict = {}
    for i in labels:
        label_dict[i["id"]] = i["caption"]
    labels = None
# encoder_input_data, target = np.array([]), np.array([])
encoder_input_data, target = [], []
for k, id in enumerate(label_dict.keys()):
    feat = np.load(path + "training_data/feat/" + id + ".npy")
    progress(k + 1, 1450, " loading data ")
    # trainFeats.append(np.load(path + "training_data/feat/" + id + ".npy"))
#     encoder_input_data.append(feat)
#     target.append(label_dict[id][0])
    for i in label_dict[id][:-5]:
        target.append(i)
        encoder_input_data.append(feat)

label_dict = None

print()
encoder_input_data = np.array(encoder_input_data)
# encoder_input_data = tf.data.Dataset.from_tensor_slices(encoder_input_data)
# encoder_input_data=np.array(encoder_input_data)

target = np.array(target)

# print('shpae of target label :',target.shape)
print("--------   successfully loading feats   --------")
# texts = set()
texts_dict = dict()
for line in target:
    line = line.lower()
    for i in line.split(" "):
        word = re.sub("[\,\.\[\]\?]", "", i)
        try:
            texts_dict[word] += 1
        except:
            texts_dict[word] = 1
        # if re.sub("[\,\.]", "", i) not in texts_dict:
        #     texts_dict.add(re.sub("[\,\.]", "", i))
print("--------   successfully finish word-embedding   --------")
# texts_dict = sorted(texts_dict.values())
texts = ['<UWK>', '<PAD>']
for key, value in texts_dict.items():
    if value > 1:
        texts.append(key)
    else:
        print(key)
        # texts[key] = None
        # del texts[key]

# print(texts)
target = np.array(target)

word2idx = dict([(word, i) for i, word in enumerate(texts)])
idx2word = dict([(i, word) for i, word in enumerate(texts)])

with open(path2+'word2idx.json', 'w') as file:
    file.write(json.dumps(word2idx))
with open(path2+'idx2word.json', 'w') as file:
    file.write(json.dumps(idx2word))
# print(word2idx)

print(word2idx)


def convert(word):
    try:
        return word2idx[re.sub("[\,\.]", "", word)]

    except:
        return word2idx['<UWK>']


decoder_target = []
for i in target:
    temp = []
    i = i.lower()
    for word in i.split(" "):
        temp.append(convert(word))
    decoder_target.append(temp)


encoder_input_shape = encoder_input_data.shape[2]
print(encoder_input_shape)
encoder_seq_len = encoder_input_data.shape[1]
decoder_seq_len = max([len(s) for s in decoder_target])
tokens_num = len(texts)
print("Number of unique output tokens:", tokens_num)
print("Sequence length for inputs:", encoder_seq_len)
print("Max sequence length for outputs:", decoder_seq_len)
texts = None

# add <PAD>
PAD = word2idx['<PAD>']
for i in decoder_target:
    for _ in range(decoder_seq_len-len(i)):
        i.append(PAD)
target = None

decoder_input_data = np.zeros(
    (len(decoder_target), decoder_seq_len, tokens_num), dtype="float32"
)
decoder_target_data = np.zeros(
    (len(decoder_target), decoder_seq_len, tokens_num), dtype="float32"
)
for i, target_text in enumerate(decoder_target):
    for t, word in enumerate(target_text):
        # decoder_input_data[i, t, word] = 1
        if t > 0:
            decoder_target_data[i, t - 1, word] = 1
print('size of input data :', sys.getsizeof(encoder_input_data))
print('shape of input data :', encoder_input_data.shape)
print('shape of target data :', decoder_input_data.shape)
print('shape of target label :', decoder_target_data.shape)

parameters = {'input1_shape': encoder_input_data.shape,
              'input2_shape': decoder_input_data.shape}
with open(path2+'parameters.json', 'w') as file:
    file.write(json.dumps(parameters))
# print(decoder_input_data)
""" model """
encoder_input = Input(shape=(None, encoder_input_shape))
# encoder = LSTM(latent_dim, return_state=True)
encoder=GRU(latent_dim,return_state=True)
#encoder_output, state_h, state_c = encoder(encoder_input)
encoder_output, state_h= encoder(encoder_input)
#encoder_states = [state_h, state_c]
encoder_states=[state_h]

decoder_input = Input(shape=(None, tokens_num))
# decoder_input = Input(shape=(None, decoder_input_data.shape[1]))
# decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True)
decoder_gru = GRU(latent_dim, return_sequences=True, return_state=True)
#decoder_outputs, _, _ = decoder_gru(decoder_input, initial_state=encoder_states)
decoder_outputs, _ = decoder_gru(decoder_input,initial_state=encoder_states)
# decoder_outputs, _, _ = decoder_lstm(
#     decoder_input, initial_state=encoder_states)
decoder_dense = Dense(tokens_num, activation="softmax")
# decoder_dense = Dense(decoder_input_data.shape[1], activation="softmax")
decoder_outputs = decoder_dense(decoder_outputs)

model = Model([encoder_input, decoder_input], decoder_outputs)

print(encoder_input_data.shape)
print(decoder_input_data.shape)
""" run training """
model.compile(optimizer="rmsprop", loss="categorical_crossentropy")
model.fit(
    [encoder_input_data, decoder_input_data],
    decoder_target_data,
    batch_size=batch_size,
    epochs=epochs,
    validation_split=0.2,
)

model.save(path2+"seq3seq.h5")

# encoder_model = Model(encoder_input, encoder_states)

# decoder_state_input_h = Input(shape=(latent_dim,))
# decoder_state_input_c = Input(shape=(latent_dim,))
# decoder_state_inputs = [decoder_state_input_h, decoder_state_input_c]
# decoder_outputs, state_h, state_c = decoder_lstm(
#     decoder_input, initial_state=decoder_state_inputs
# )
# decoder_states = [state_h, state_c]
# decoder_outputs = decoder_dense(decoder_outputs)
# decoder_model = Model(
#     [decoder_input] + decoder_state_inputs, [decoder_outputs] + decoder_states
# )
