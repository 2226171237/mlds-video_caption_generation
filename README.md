# Video Caption Generation
To turn a short video into 
the corresponding caption that depicts the video 
using sequence-to-sequence model.  

example : 

![](MLDS_hw2_1_data/images_for_md/1.gif)  ->  "A train rides over a bridge."

***
## sequence-to-sequence model
![](MLDS_hw2_1_data/images_for_md/3.gif)  
ref: http://jalammar.github.io/visualizing-neural-machine-translation-mechanics-of-seq2seq-models-with-attention/

## my model
![](model.png)  
lstm_1 is the encoder-RNN  
lstm_2 is the decoder-RNN
***
## resuls
using LSTM : Average bleu score is 0.46902394135556763  
using GRU  : Average bleu score is 0.4919565327595585 

## example outputs
- good cases  
  1. TZ860P4iTaM_15_28.avi,Cat is playing a piano  
![](MLDS_hw2_1_data/images_for_md/4.gif)  
  2. qvg9eM4Hmzk_4_10.avi,Man is lifting a car  
  ![](MLDS_hw2_1_data/images_for_md/5.gif)
- bad cases  
  1. lw7pTwpx0K0_38_48.avi,Man is a a .  
  2. HAjwXjwN9-A_16_24.avi,Woman is a a .
  3. Je3V7U5Ctj4_569_576.avi,Man is a a a a .

## how to improve my model
- Attention model  
- teacher forceing
***
## the complete report is here  
https://shenperson.github.io/MLDS_report/

## reference
- keras-team : https://github.com/keras-team/keras
- visualize the model : http://jalammar.github.io/visualizing-neural-machine-translation-mechanics-of-seq2seq-models-with-attention
